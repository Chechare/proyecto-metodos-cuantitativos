/** Class representing a Server */
class Server{
  /**
  * Creates a Server.
  * @param {string} [name] - Server's name.
  */
  //The server constructor
  constructor(name){
    this.name = 'Server';
    if(name){
      this.name = name;
    }
    this.busy = false;
    this.timeBusy = 0;
    this.timeOfService = 0;
    this.client = null;
    this.clientsAttended = 0;
  }
  /**
  * Function to assign clients.
  * @param {number} [client] - The client.
  * @param {number} [timeOfService] - The time of service.
  */
  assignClient(client, timeOfService){
    this.client = client;
    this.timeOfService = timeOfService;
    this.busy = true;
  }

}
