/** Class representing a Queue of the system. */
class Queue{
  /**
  * Creates a Queue object.
  * @param {number} [capacity] = Capacity of the Queue.
  * @param {Client[]} [clients] = An array with the clients that are in the Queue.
  */
  //The constructor of the queue
  constructor(capacity){
    this.capacity = Infinity;
    this.clients = [];

    if(capacity){
      this.capacity = capacity;
    }
  }

  /**
  * Gets the next client in the Queue (FIFO).
  * @return {Client} - The next client. If there is no clients left it returns undefined.
  */
  dequeue(){
    return this.clients.shift();
  }

  /**
   * enqueue - Insert a client at the in of the Queue (FIFO).
   *
   * @param  {Client} client - Client to insert.
   * @return {Client} - The inserted client.
   */
  enqueue(client){
    return this.clients.push(client);
  }

  //Check if there is a available space on the queue
  checkAvailable(){
    return (this.capacity - this.clients.length) > 0;
  }

    /**
   * addClients - Insert a client at the in of the Queue (FIFO).
   *
   * @param  {string} client - Client to insert.
   */
  addClients(clients){
    while(clients.length > 0 && this.checkAvailable()){
      this.clients.push(clients.shift());
    }

    return clients.length;
  }
  //Returns the length of the client list.
  length(){
    return this.clients.length;
  }
}
