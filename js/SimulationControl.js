/** Class representing the control of the simulation */
class SimulationControl {
	  /**
  * Creates a constructor.
  * @param {number} [numServer] = The number of the servers.
  * @param {number} [lambda] = The lambda of the simulation.
  * @param {number} [mu] = The mu of the simulation.
  * @param {number} [capacity] = Capacity of the Queue.

  */

	constructor(numServer, lambda, mu, capacity, timeLimit, callback){
		this.numServer = numServer;
		this.lambda = lambda;
		this.mu = mu;
		this.capacity = capacity;
		this.dropped = 0;
		this.attended = 0;
		this.attendedTime = 0;
		this.time = 0;
		this.timeLimit = timeLimit;
		this.callback = callback;
		this.queue = new Queue(capacity);
		this.servers = [];
		this.waiting = 0;
		this.timeWaiting = 0;

		this.r = new Rands();

		for(var i = 0; i < this.numServer; i++){
			this.servers.push(new Server('Server ' + i));
		}
	}

	/**
	* Creates a Client array.
	* @param {number} [poissonNumber] = Creates a poisson number of clients to assign.
	*/
	createClients(poissonNumber){
		var clients = [];
		for(var i = 0; i < poissonNumber; i++){
			clients.push(new Client());
		}

		return clients;
	}

	/**
	 * update - Advances simulation one unit of time.
	 *
	 */
	update(){
		if(this.time == this.timeLimit){
			clearInterval(this.callback);
		}

		console.log(this);
		var busy = 0;

		//Check if there are clients to left.
		for(var i = 0; i < this.numServer; i++){
			if(this.servers[i].busy){
				busy++;
				//Check the time of each server
				this.attendedTime += this.servers[i].timeOfService;
				this.servers[i].timeOfService--;
				this.servers[i].client.timeOnSystem++;

				if(this.servers[i].timeOfService < 0){
					this.attended++;
					this.servers[i].client = null;
					this.drawer.eventClientOut(i);
					this.servers[i].busy = false;
				}
			}
		}

		//Generate new clients.
		var poissonNumber = this.r.poisson(this.lambda);
		var newClients = this.createClients(poissonNumber);


		var droppedNow = this.queue.addClients(newClients);
		this.dropped += droppedNow;
		this.drawer.eventClientEnque(poissonNumber - droppedNow);

		console.log("Generated: " + poissonNumber);
		console.log("Busy: " + busy);
		console.log("In QUEUE:" + this.queue.length());

		for(var i = 0; i < droppedNow; i++){
			this.drawer.eventDropClient();
		}


		if(this.queue.length() > 0){
			for(var i = 0; i < this.numServer && this.queue.length() > 0 ; i++){
				if(!(this.servers[i].busy)){
					//Generates exponential
					var timeOfService = this.r.exponential(this.mu);
					this.servers[i].assignClient(this.queue.dequeue(), timeOfService);
					this.drawer.eventClientDeque(i);
					this.servers[i].client.timeOnSystem++;
					this.servers[i].clientsAttended++;
					this.servers[i].timeBusy+= timeOfService;
				}
			}
		}

		//Check the length of the clients and sums the time on the queue and on the system.
		for(var i = 0; i < this.queue.length(); i++){
			this.queue.clients[i].timeQueued++;
			this.timeWaiting += this.queue.clients[i].timeQueued;
			this.queue.clients[i].timeOnSystem++;
		}
		this.waiting += this.queue.length();

		this.time++;
	}
}
