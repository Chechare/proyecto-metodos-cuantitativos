/*  modelFunctions.js
*
*   Main functions for the model queue calculus M/M/m.
*/

/** Calculates the utilization factor of the system.
* @param {number} lampda - Lambda value.
* @param {number} mu - mu value.
* @return {number} - Utilization factor of the system.
*/
function utilization_factor(lambda, mu){
  return lambda / mu;
}

/**
* Calculates the probability of n clients in the system.
* @param {number} p - Utilization factor.
* @param {number} n - Number of clients.
* @return {number} - Probability of n clients in the system.
*/
function clients_probability(p, n){
  return Math.pow(p, n) * (1 - p);
}

/**
* Calculates the average amount of clients in a queue
* @param {number} lambda - Lambda value.
* @param {number} mu - mu value.
* @return {number} - The average amount of clients in a queue.
*/
function queue_clients_avg(lambda, mu){
  return ((lambda * lambda) / mu) * (mu - lambda);
}

/**
* Calculates the average waiting time of a client in a queue.
* @param {number} lambda - Lambda value.
* @param {number} mu - mu value.
* @return {number} - The average waiting time of a client in a queue.
*/
function queue_time_avg(lambda, mu){
  var lq = queue_clients_avg(lambda, mu);
  return lq / lambda;
}

/**
* Calculates the average waiting time of a client in a system.
* @param {number} lambda - Lambda value.
* @param {number} mu - mu value.
* @return {number} - The average waiting time of a client in a system.
*/
function system_time_avg(lambda, mu){
  var wq = queue_time_avg(lambda, mu);
  return wq + 1/mu;
}

/**
* Calculates the average number of clients in a system.
* @param {number} lambda - Lambda value.
* @param {number} mu - mu value.
* @return {number} - The average number of clients in a system.
*/
function system_clients_avg(lambda, mu){
  var w = system_time_avg(lambda, mu);
  return lambda * w;
}
