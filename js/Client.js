/** Class representing a Client */
class Client {

  /**
  * Creates a Client.
  * @param {string} [name] = Client's name.
  */

  //The client's constructor
  constructor(name){
    this.name = 'Client';
    if(name){
      this.name = name;
    }

    this.timeQueued = 0;
    this.timeOnSystem = 0;
  }
}
