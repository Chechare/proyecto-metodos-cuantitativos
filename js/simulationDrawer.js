class SimulationDrawer {

  /**
   * constructor - Create a SimulationDrawer
   *
   * @param  {type} two description
   * @return {SimulationDrawer}     created object.
   */
  constructor(two) {
    this.two = two;
    this.serverGroup = two.makeGroup();
    this.clientGroup = two.makeGroup();
    this.clients = [];
    this.servers = [];
    this.radious = 30;
    this.nx = 0;
    this.n = 0;
  }

  /**
   * eventDropClient - Tells drawer to animate a client drop.
   *
   */
  eventDropClient(){
    this.eventClientEnque(1);
    this.moveClientToOut(this.clients[this.clients.length-1]);
  }

  /**
   * eventClientDeque - Tells drawer to animate a client going to a server.
   *
   * @param  {Number} serverId - server index.
   */
  eventClientDeque(serverId){
    var clientId = -1;
    for (var i = 0; i < this.clients.length; i++) {
      var elem = this.clients[i];
      if(!elem.movedServer){
        clientId = i;
        break;
      }
    }
    if(clientId == -1){ return;}
    this.moveClientToServer(this.clients[clientId], this.servers[serverId]);
  }

  /**
   * eventClientOut - Tells drawer to animate client exiting system.
   *
   * @param  {Number} serverId - Server that finnished attending client.
   */
  eventClientOut(serverId){
    if (this.servers[serverId].clientInServer){
      this.moveClientToOut(this.servers[serverId].clientInServer);
      this.servers[serverId].clientInServer = undefined;
    }
  }

  /**
   * eventClientEnque - Tells drawer to animate client arrival to queue.
   *
   * @param  {Number} n     - Amount of clients.
   * @param  {Array} names - Array of names, if available.
   */
  eventClientEnque(n,names){
    var self = this;
    this.n++;
    for (var i = 0; i < n; i++) {
      var lastindx = -1;
      this.clients.forEach(function(elem, index){
        if(!elem.movedServer) lastindx = index;
      });
      var last, cx, cy, back,row;
      if (lastindx >= 0){ //someone is in queue
        last = this.clients[lastindx];
        cx = last.translation.x;
        cy = last.translation.y;
        row = this.clients.length/this.nx;
        row = (cy - (this.radious*2.5-two.height/2))/(this.radious*2);
        back = row%2;
        cx += (back?1:-1)*this.radious*2;
        if(cx > -self.radious*2 || cx < -this.nx*this.radious*2 -this.radious*2 ){
          cx = last.translation.x;
          cy += this.radious*2;
        }
      }
      else{ //no one in queue
        cx = -self.radious*2;
        cy = this.radious*2.5-two.height/2;;
      }
      var circle = two.makeCircle(0,0,this.radious);
      circle.fill = '#FF8000';
      circle.stroke = 'orangered';
      circle.linewidth = 5;
      var indexN = this.n-1;
      var name = names? names[i]|| 'Client':'Client';
      console.log(indexN);
      var text = new Two.Text(name+indexN, 0, 0);
      text.size = self.radious/2;
      text.fill = 'white';
      text.weight = 'bolder';

      var client = two.makeGroup().add(circle).add(text);
      client.translation.x = cx;
      client.translation.y = cy;
      self.clients.push(client);
      self.clientGroup.add(client);
    }
  }
  /**
   * moveClientToServer - Animate client to server position.
   *
   * @param  {Shape} client - shape representing client.
   * @param  {Shape} server - shape representing server.
   */
  moveClientToServer(client, server){
    var self = this;
    server.clientInServer = client;
    client.movedServer = true;

    var position = {
      x : client.translation.x,
      y : client.translation.y
    };
    var target = {
      x : server.translation.x - this.radious/3 + this.serverGroup.translation.x -this.clientGroup.translation.x,
      y : server.translation.y + this.serverGroup.translation.y -this.clientGroup.translation.y
    };
    var tween = new TWEEN.Tween(position).to(target, 1000)
    .onUpdate(function() {
      client.translation.x = position.x;
      client.translation.y = position.y;
    })
    .easing(TWEEN.Easing.Quadratic.Out)
    .start();
  }
  /**
   * moveClientToOut - Animate client outside screen and remove it.
   *
   * @param  {Shape} client - client to remove
   */
moveClientToOut(client){
    client.moveServer = true;
    var self = this;
    var position = {
      x : client.translation.x,
      y : client.translation.y
    };
    var target = {
      x : this.two.width + 2*this.radious,
      y : client.translation.y
    };
    var tween = new TWEEN.Tween(position).to(target, 2000)
    .onUpdate(function() {
      client.translation.x = position.x;
    })
    .onComplete(function(){
      var index = self.clients.indexOf(client);
      if(index == -1){
        console.log("Index error");
        return;
      }
      self.clients.splice(index,1);
      self.clientGroup.remove(client);
    })
    .easing(TWEEN.Easing.Back.In)
    .start();
  }

  /**
   * start - tells drawer to start animating.
   *
   */
  start(){
    this.two.play();
  }

  /**
   * stop - tells drawer to stop animating.
   *
   * @return {type}  description
   */
  stop(){
    this.two.pause();
  }

  /**
   * animate - Main animation loop.
   *
   */
  animate(){
    TWEEN.update();
    var two = this.two;
    //Responsive adjustments
    this.serverGroup.translation.x = 3*two.width/4;
    this.serverGroup.translation.y = two.height/2;
    this.clientGroup.translation.x = 3*two.width/4 - 4*this.radious;
    this.clientGroup.translation.y = two.height/2;
    this.recalculateQueue();
  }

  /**
   * recalculateQueue - Recalculates client positions inside Queue.
   *
   */
  recalculateQueue(){
    var self = this;
    var cy = this.radious*2.5-two.height/2;
    var cl = 0;
    var back = false;
    var cx = 0;
    this.clients.forEach(function(elem, index){
      if(elem.movedServer) return;
      if (cl > self.nx ){
        cl = 0;
        cy+= self.radious*2;
        back = !back;
      }
      else{
        cx += self.radious*2*(back?1:-1);
      }
      cl++;
      elem.translation.x = cx;
      elem.translation.y = cy;
    });
  }

  /**
   * init - Initializes values from a simulation object
   *
   * @param  {Simulation} simulation - Simualation to draw.
   */
  init(simulation){
    var two = this.two;
    var r = this.radious;
    var self = this;
    this.nx = parseInt((3*two.width/4 - 5*this.radious)/(self.radious*2)) - 1;
    var cy = this.radious*2.5-two.height/2;
    var cl = 0;
    var back = false;
    var cx = 0;
    simulation.queue.clients.forEach(function(elem, index){
      self.n++;
      if (cl > self.nx ){
        cl = 0;
        cy+= self.radious*2;
        back = !back;
      }
      else{
        cx += self.radious*2*(back?1:-1);
      }
      cl++;
      var circle = two.makeCircle(0,0,r);
      circle.fill = '#FF8000';
      circle.stroke = 'orangered';
      circle.linewidth = 5;

      var text = new Two.Text(elem.name+index, 0, 0);
      text.size = self.radious/2;
      text.fill = 'white';
      text.weight = 'bolder';

      var client = two.makeGroup().add(circle).add(text);
      client.translation.x = cx;
      client.translation.y = cy;
      self.clients.push(client);
      self.clientGroup.add(client);
    });
    simulation.servers.forEach(function(elem,index){
      var offset = self.radious * 2.5;
      var lower = -offset*simulation.servers.length/2;
      var y = lower+offset*index;

      var rect = two.makeRectangle(0, 0, self.radious*2, self.radious*2);
      rect.fill = 'YellowGreen';
      rect.stroke = 'LimeGreen';
      rect.linewidth = 5;

      var text = new Two.Text(index, 0, 0);
      text.size = self.radious/1.5;
      text.fill = 'white';
      text.weight = '700';
      var server = two.makeGroup().add(rect).add(text);
      server.translation.y = y;
      self.servers.push(server);
      self.serverGroup.add(server);
    })
    two.bind('update', function(frameCount){
      self.animate();
    });
  }
}
